
    /*para los tooltip se debe definir un script que indique la activacion dentro de la pagina*/
      $(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
      });
      $('#carouselControls').carousel({
          interval: 2000
        });
      $('#contacto').on('show.bs.modal', function(e){
        console.log('el modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-dark');
        $('#contactoBtn').prop('disabled',true);
      });
      $('#contacto').on('shown.bs.modal', function(e){
        console.log('el modal contacto se mostró');
      });
       $('#contacto').on('hide.bs.modal', function(e){
        console.log('el modal de contato se oculta');
      });
      $('#contacto').on('hidden.bs.modal', function(e){
        console.log('el modal contacto se ocultó');
        $('#contactoBtn').prop('disabled',false);
        $('#contactoBtn').removeClass('btn-dark');
        $('#contactoBtn').addClass('btn-outline-success');
      });